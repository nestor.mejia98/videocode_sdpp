﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VideoEncoderLibrary
{
    public class VideoEncoder
    {

        //1 - Definir un evento
        //2 - Definir un delegado y su handler
        //3 - Disparar el evento

        public delegate void VideoEncoderEventHandler(object sender);

        public event VideoEncoderEventHandler OnVideoEncoded;
        

        public void Encode(Video video) {

            Console.WriteLine("Encoding...");
            OnVideoEncoded(this);

        }

    }
}
