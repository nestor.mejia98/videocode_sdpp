﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VideoEncoderLibrary;

namespace VideoCoderConsole
{
    class Program
    {
        static void Main(string[] args)
        {

            var video = new Video() { Title = "My video" };
            var encoder = new VideoEncoder();

            var mailService = new MailService();
            var chargeService = new ChargeService();

            encoder.OnVideoEncoded += mailService.HandlerEvento;
            encoder.OnVideoEncoded += chargeService.Charge;

            encoder.Encode(video);

        }

        public class MailService
        {

            public void HandlerEvento(object sender) {

                Console.WriteLine("Executing handler on finish");

            }

        }

        public class ChargeService {

            public void Charge(object sender) {
                Console.WriteLine("Charged");
            }

        }

    }
}
